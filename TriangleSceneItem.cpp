#include <QtGui/QOpenGLContext>
#include <QtQuick/QQuickWindow>
#include <QTimer>

#include "TriangleScene.h"
#include "TriangleSceneItem.h"

TriangleSceneItem::TriangleSceneItem()
    : mScene(0)
{
    mScene = new TriangleScene();

    mTime.start();

    connect(this,SIGNAL(windowChanged(QQuickWindow*)),this,SLOT(handleWidowChanged(QQuickWindow*)),Qt::DirectConnection);
}

void TriangleSceneItem::handleWidowChanged(QQuickWindow *win)
{
    if(win)
    {
        QSurfaceFormat f = win->format();
        f.setMajorVersion(4);
        f.setMinorVersion(3);
        f.setSamples(4);
        f.setStencilBufferSize(8);
        f.setProfile(QSurfaceFormat::CompatibilityProfile);

        win->setFormat(f);

        win->setClearBeforeRendering(false);

        connect(win,SIGNAL(beforeSynchronizing()),this,SLOT(sync()),Qt::DirectConnection);
        connect(win,SIGNAL(sceneGraphInvalidated()),this,SLOT(cleanup()),Qt::DirectConnection);
        connect(win,SIGNAL(beforeRendering()),mScene,SLOT(paint()),Qt::DirectConnection);
    }
}

void TriangleSceneItem::cleanup()
{
    if(mScene)
    {
        delete mScene;
        mScene = 0;
    }
}


void TriangleSceneItem::sync()
{
    if(mScene)
    {
        QTimer *timer = new QTimer();
        connect(timer,SIGNAL(timeout()),this,SLOT(updateTime()),Qt::DirectConnection);
        timer->start();

        mScene->setViewportSize(window()->width() * window()->devicePixelRatio(),
                                window()->height() * window()->devicePixelRatio());
    }
}


void TriangleSceneItem::updateTime()
{
    float time = mTime.elapsed() * 0.001;

    mScene->update(time);

    //ask QQuickView to render the scene
    window()->update();
}
