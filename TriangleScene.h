#ifndef TRIANGLESCENE_H
#define TRIANGLESCENE_H

#include <QOpenGLFunctions_4_3_Compatibility>

#include "defines.h"
#include "AbstractScene.h"

class GLSLShader;

class TriangleScene : public AbstractScene, protected QOpenGLFunctions_4_3_Compatibility
{
    Q_OBJECT

public:
    explicit TriangleScene(QObject *parent = 0);
    ~TriangleScene();

    //scene related functions
    virtual void initialise();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w,int h);


    void setViewportSize(int width,int height);

public slots:

    void paint();

private:

    void loadShaders();

    GLSLShader *mTriangleShader;

    GLuint mVaoID;
    GLuint mVerticesID;
    GLuint mIndicesID;

    int mWinWidth;
    int mWinHeight;

    struct Vertex
    {
        glm::vec3 position;
        glm::vec3 color;
    };

    Vertex mVertices[3];

    GLushort mIndices[3];

    //projection and model-view matrix
    glm::mat4 mModelViewMatrix;
    glm::mat4 mProjectionMatrix;

    bool mInitialized;

    float mCurrentTime;
};

#endif // TRIANGLESCENE_H
