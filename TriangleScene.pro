include ( common/common.pri )

TEMPLATE = app

INCLUDEPATH += common

OTHER_FILES += shaders/triangle.vert \
               shaders/triangle.frag

SOURCES += \
    main.cpp \
    TriangleScene.cpp \
    TriangleSceneItem.cpp

HEADERS += \
    TriangleScene.h \
    TriangleSceneItem.h

RESOURCES += \
    TriangleScene.qrc
