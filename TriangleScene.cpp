#include <QTimer>

#include "TriangleScene.h"
#include "GLSLShader.h"

TriangleScene::TriangleScene(QObject *parent)
    : AbstractScene(parent),
      mInitialized(false),
      mTriangleShader(0),
      mCurrentTime(0.0f)
{

}

TriangleScene::~TriangleScene()
{
    if(mTriangleShader)
    {
        mTriangleShader->DeleteShaderProgram();
        delete mTriangleShader;
        mTriangleShader = 0;
    }

    glDeleteBuffers(1,&mVerticesID);
    glDeleteBuffers(1,&mIndicesID);
    glDeleteVertexArrays(1,&mVaoID);
}

void TriangleScene::initialise()
{
    //return if already initialized
    if(mInitialized)
        return;

    if(!initializeOpenGLFunctions())
    {
        std::cerr << "Modern OpenGL Functions could not be initialized." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    mProjectionMatrix = glm::mat4(1.0f);
    mModelViewMatrix = glm::mat4(1.0f);

    loadShaders();

    //setup the triangle geometry
    mVertices[0].color = glm::vec3(1.0,0.0,0.0);
    mVertices[1].color = glm::vec3(0.0,1.0,0.0);
    mVertices[2].color = glm::vec3(0.0,0.0,1.0);

    mVertices[0].position = glm::vec3(-1,-1,0);
    mVertices[1].position = glm::vec3(0,1,0);
    mVertices[2].position = glm::vec3(1,-1,0);

    //setup the triangle indices
    mIndices[0] = 0;
    mIndices[1] = 1;
    mIndices[2] = 2;

    //check for opengl errors
    GL_CHECK_ERRORS;

    //setup the triangle vao and vbo stuff
    glGenVertexArrays(1,&mVaoID);

    glGenBuffers(1,&mVerticesID);
    glGenBuffers(1,&mIndicesID);

    //get the size of the Vertex struct
    GLsizei stride = sizeof(Vertex);

    glBindVertexArray(mVaoID);

    glBindBuffer(GL_ARRAY_BUFFER,mVerticesID);
    //pass triangle vertices to the buffer object
    glBufferData(GL_ARRAY_BUFFER,sizeof(mVertices),&mVertices[0],GL_STATIC_DRAW);

    GL_CHECK_ERRORS;

    glEnableVertexAttribArray(mTriangleShader->getAttribute("vVertex"));
    glVertexAttribPointer(mTriangleShader->getAttribute("vVertex"),3,GL_FLOAT,GL_FALSE,stride,0);

    GL_CHECK_ERRORS;

    //enable vertex attribute array for the color
    glEnableVertexAttribArray(mTriangleShader->getAttribute("vColor"));
    glVertexAttribPointer(mTriangleShader->getAttribute("vColor"),3,GL_FLOAT,GL_FALSE,stride,(const GLvoid*)offsetof(Vertex,color));

    GL_CHECK_ERRORS;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mIndicesID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(mIndices),&mIndices[0],GL_STATIC_DRAW);

    glDisableVertexAttribArray(mTriangleShader->getAttribute("vVertex"));
    glDisableVertexAttribArray(mTriangleShader->getAttribute("vColor"));
    glBindVertexArray(0);

    GL_CHECK_ERRORS;

    mInitialized = true;

}

void TriangleScene::resize(int w, int h)
{
    if(h < 1)
        h = 1;

    mWinWidth = w;
    mWinHeight = h;

    glViewport(0,0,(GLsizei)mWinWidth,(GLsizei)mWinHeight);

    mProjectionMatrix = glm::ortho(-1,1,-1,1);
}

void TriangleScene::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDisable(GL_DEPTH_TEST);

    mTriangleShader->Use();

    glUniformMatrix4fv(mTriangleShader->getUniform("MVP"),1,GL_FALSE,glm::value_ptr(mProjectionMatrix * mModelViewMatrix));

    glBindVertexArray(mVaoID);
    glEnableVertexAttribArray(mTriangleShader->getAttribute("vVertex"));
    glEnableVertexAttribArray(mTriangleShader->getAttribute("vColor"));

    //draw the triangle
    glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_SHORT,0);

    glDisableVertexAttribArray(mTriangleShader->getAttribute("vVertex"));
    glDisableVertexAttribArray(mTriangleShader->getAttribute("vColor"));
    glBindVertexArray(0);

    mTriangleShader->UnUse();

}

void TriangleScene::update(float t)
{
    mCurrentTime = t;
}

void TriangleScene::paint()
{
    if(!mInitialized)
        initialise();

    resize(mWinWidth,mWinHeight);
    render();
}

void TriangleScene::loadShaders()
{
    mTriangleShader = new GLSLShader();

    mTriangleShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/triangle.vert");
    mTriangleShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/triangle.frag");

    mTriangleShader->CreateAndLinkProgram();

    mTriangleShader->Use();

    mTriangleShader->AddAttribute("vVertex");
    mTriangleShader->AddAttribute("vColor");
    mTriangleShader->AddUniform("MVP");

    mTriangleShader->UnUse();
}

void TriangleScene::setViewportSize(int width, int height)
{
    mWinWidth = width;
    mWinHeight = height;
}
