#ifndef TRIANGLESCENEITEM_H
#define TRIANGLESCENEITEM_H

#include <QTime>
#include <QtQuick/QQuickItem>

class TriangleScene;

class TriangleSceneItem : public QQuickItem
{
    Q_OBJECT

public:

    TriangleSceneItem();

public slots:

    void updateTime();
    void sync();
    void cleanup();

private slots:
    void handleWidowChanged(QQuickWindow *);

private:

    TriangleScene *mScene;

    QTime mTime;

};

#endif // TRIANGLESCENEITEM_H
