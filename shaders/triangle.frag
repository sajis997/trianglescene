#version 430 compatibility


layout (location = 0) out vec4 vFragColor;

smooth in vec4 vSmoothColor;

void main()
{
    vFragColor = vSmoothColor;
}
