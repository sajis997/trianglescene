#version 430 compatibility

layout(location = 0) in vec3 vVertex;
layout(location = 0) in vec3 vColor;

smooth out vec4 vSmoothColor;

//uniform
uniform mat4 MVP;

void main()
{
    vSmoothColor = vec4(vColor,1.0);

    //convert to the clip space coordinate
    gl_Position = MVP * vec4(vVertex,1.0);
}
