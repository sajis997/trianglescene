QT += core gui qml quick widgets

HEADERS += $$PWD/AbstractScene.h \
           $$PWD/defines.h \
           $$PWD/GLSLShader.h

SOURCES += $$PWD/AbstractScene.cpp \
           $$PWD/GLSLShader.cpp
